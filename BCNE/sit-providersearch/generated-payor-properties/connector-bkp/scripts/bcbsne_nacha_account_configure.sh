#!/bin/bash
set -e

HE_DIR=$1
HOST_NAME=$2

PROPERTY_FILE=./bcbsne_env.properties

function getProperty {
   PROP_KEY=$1
   PROP_VALUE=`
   cat $PROPERTY_FILE | grep "$PROP_KEY" | cut -d'=' -f2`
   echo $PROP_VALUE
}

echo ""
echo "********** BCBSNE SH : bcbsne_nacha_account_configure.sh start on ${HOST_NAME} **********"


    echo "Start configuring nacha_account trigger route"

        nachaAccountJsonRouteFile=$HE_DIR/bcbsne-nachaaccount/resources/config/nachaaccount-route-config.json

        extractCron=$(getProperty nacha_account_extract_job_frequency)
        echo "extractCron: ${extractCron}"

        sed -i -e "s|#EXTRACT_TIME#|${extractCron}|g" $nachaAccountJsonRouteFile
        
        schedulerStagingDir=$(grep "stagingDir" $HE_DIR/etc/com.healthedge.customer.generic.scheduler.cfg | sed -e "s/stagingDir=//g")
        mkdir -p $schedulerStagingDir

        fileRequestBaseDir=$(grep "fileRequestBaseDir" $HE_DIR/etc/com.healthedge.customer.generic.scheduler.cfg | sed -e "s/fileRequestBaseDir=//g")
        fileRequestBaseDir=${fileRequestBaseDir}/request/cron

        mkdir -p $fileRequestBaseDir

        echo "Dropping nacha_account job trigger creation json ${nachaAccountJsonRouteFile} to ${fileRequestBaseDir}"
        cp $nachaAccountJsonRouteFile $fileRequestBaseDir/nachaaccount-route-config.json

    echo "End configuring nacha_account job trigger route"


echo "********** BCBSNE SH : bcbsne_nacha_account_configure.sh end on ${HOST_NAME} **********"
echo ""
